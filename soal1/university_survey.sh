#!/bin/bash
#1a
echo ==========1a==========
grep "Japan" '2023 QS World University Rankings.csv'| sort -t , -k1 -n| head -n 5| awk -F , '{print $1, $2, $4}'
echo 

#1b
echo ==========1b==========
cat '2023 QS World University Rankings.csv' | grep "Japan" | sort -t , -k9 -n| head -n 5| awk -F , '{print $1, $2, $4, $9}'
echo 

#1c
echo ==========1c==========
cat '2023 QS World University Rankings.csv' | grep "Japan" | sort -t , -k20 -n| head -n 10| awk -F , '{print $1, $2, $4, $20}'
echo 

#1d
echo ==========1d==========
cat '2023 QS World University Rankings.csv' | awk -F , '/Keren/ {print $1, $2}'
echo
