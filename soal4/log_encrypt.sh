#!/bin/bash

mkdir -p "encrypted_backup"

uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

curr_hour=$(date "+%H")
curr_date=$(date "+%H:%M %d:%m:%Y")

cd encrypted_backup

backup_filename="${curr_date}.txt"

cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${curr_hour}:26}${uppercase:${curr_hour}:26}" > "${backup_filename}"

# Crontab
# 0 */2 * * * /bin/bash /home/arvianta/sisop/P1/NO4/log_encrypt.sh
