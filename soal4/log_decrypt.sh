#!/bin/bash

mkdir -p "decrypted_backup"

uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

read -p "Insert file name : " filename

filehour=$(echo "$filename" | cut -d ':' -f1) 

cd encrypted_backup

encrypted_path="$(pwd)"/"${filename}"

cd ..

cd decrypted_backup

cat "${encrypted_path}" | tr "${lowercase:${filehour}:26}${uppercase:${filehour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${filename}"