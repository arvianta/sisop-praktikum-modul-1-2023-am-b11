#!/bin/bash

mkdir -p "users"

list_user="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

echo "=====Registration System====="
echo "Input new username and password"

read -p "Username: " username

read -p "Password: " password

if [[ "$(grep "$username" "$list_user")" ]]; then
    echo -e "\nUsername already registered"
    log "ERROR" "User already exists"
  exit 1

elif [[ "${#password}" -lt 8 ]]; then
    echo -e "\nPassword must contain at least 8 characters"
  exit 1

elif [[ ! $(echo "$password" | grep '[A-Z]' | grep '[a-z]') ]]; then
    echo -e "\nPassword must contain at least 1 uppercase and 1 lowercase alphabet"
  exit 1

elif [[ "$password" =~ [^a-zA-Z0-9] ]]; then
    echo -e "\nPassword must only contain alphanumeric characters"
  exit 1

elif [[ "$password" == "$username" ]]; then
    echo -e "\nPassword and username cannot be the same"
  exit 1

elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
    echo -e "\nPassword cannot contain the word 'chicken' or 'ernie'"
  exit 1

else
  echo "$username:$password" >> "$list_user"
  echo -e "\nAccount has been created"
  log "INFO" "User $username registered succesfully"
fi