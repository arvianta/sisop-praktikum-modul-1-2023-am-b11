#!/bin/bash

list_user="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

echo "=====Login System====="
echo "Input Username and Password"

read -p "Username: " username
read -p "Password: " password

if [[ ! "$(grep "$username" "$list_user")" ]] ; then
    echo -e "\nIncorrect Username or password"
  exit 1
fi

if [[ "$(grep "$username" "$list_user" | cut -d ':' -f 2)" == "$password" ]] ; then
    echo -e "\nLogin Successful"
    log "INFO" "User $username logged in"
else
    echo -e "\nLogin Failed"
log "ERROR" "Failed attempt on user $username"
  exit 1
fi
