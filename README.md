# Praktikum Sistem Operasi Modul 1 
Anggota Kelompok ``B11`` 
| Nama                      | NRP        |
|---------------------------|------------|
| Rayhan Arvianta Bayuputra | 5025211217 |
| Danno Denis Dhaifullah    | 5025211027 |
| Samuel Berkat Hulu*       | 5025201055 |

ps: *tidak ikut mengerjakan Praktikum 

## Nomor 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

### Soal A
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. **_Tampilkan 5 Universitas dengan ranking tertinggi di Jepang._**

**Solusi :**
```sh
grep "Japan" '2023 QS World University Rangkings.csv' | sort -t , -k1 -n | head -n 5 | awk -F , '{print $1, $2, $4}'
```
**Penjelasan Kode :**
- ``grep`` merupakan command-line utility di Linux yang digunakan untuk mencari pola dari sebuah text file. Pola yang kita cari dari file ``2023 QS World University Rangkings.csv`` adalah string yang mengandung kata ``"Japan"``.
- ``sort`` merupakan command pada Linux yang digunakan untuk mengurutkan barisan pada text file pada urutan tertentu. ``-t ,`` merupakan command yang terdapat pada ``sort`` yang menandakan bahwa data dipisah oleh separator comma. ``-k1`` berarti melakukan sorting dari seluruh kolom 1 pada .csv filenya. ``-n`` artinya melakukan sorting secara numerikal, dan secara default akan melakukannya secara ascending.
- ``head`` merupakan command pada Linux untuk menampilkan beberapa line pertama dari sebuah file atau command. ``-n 5`` disini berarti mengambil 5 baris pertama dari input.
- ``awk`` merupakan command pada Linux untuk memproses dan manipulasi text file. ``-F ,`` merupakan command pada ``awk`` untuk mendetermine separator dari data yang terdapat pada file dan separator disini adalah comma. Kemudian ``print`` digunakan untuk menampilakan data sesuai dengan kondisi-kondisi yang ada, ``$1, $2, $4`` berarti mengambil data dari kolom ke-1, 2, dan 4.

### Soal B
Karena Bocchi kurang percaya diri dengan kemampuannya, **_coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang._**

**Solusi :**
```sh
cat '2023 QS World University Rankings.csv' | grep "Japan" | sort -t , -k9 -n| head -n 5| awk -F , '{print $1, $2, $4, $9}'
```
**Penjelasan Kode :**
- ``cat`` merupakan command-line utility pada Linux yang digunakan untuk menampilkan isi pada suatu file. ``cat`` disini digunakan untuk mengakses data pada ``2023 QS World University Rankings.csv``
- ``grep`` merupakan command-line utility di Linux yang digunakan untuk mencari pola dari sebuah text file. Disini kita mencari string ``"Japan"`` dari file .csv yang sebelumnya kita akses menggunakan ``cat``.
- ``sort`` merupakan command pada Linux yang digunakan untuk mengurutkan barisan pada text file pada urutan tertentu. ``-t ,`` merupakan command yang terdapat pada ``sort`` yang menandakan bahwa data dipisah oleh separator comma. ``-k9`` berarti melakukan sorting dari seluruh kolom 9 pada .csv filenya. ``-n`` artinya melakukan sorting secara numerikal, dan secara default akan melakukannya secara ascending.
- ``head`` merupakan command pada Linux untuk menampilkan beberapa line pertama dari sebuah file atau command. ``-n 5`` disini berarti mengambil 5 baris pertama dari input.
- ``awk`` merupakan command pada Linux untuk memproses dan manipulasi text file. ``-F ,`` merupakan command pada ``awk`` untuk mendetermine separator dari data yang terdapat pada file dan separator disini adalah comma. Kemudian ``print`` digunakan untuk menampilakan data sesuai dengan kondisi-kondisi yang ada, ``$1, $2, $4, $9`` berarti mengambil data dari kolom ke-1, 2, 4 dan 9.

### Soal C
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, **_cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi_**

**Solusi :**
```sh
cat '2023 QS World University Rangkings.csv' | grep "Japan" | sort -t , -k20 -n| head -n 10| awk -F , '{print $1, $2, $4, $20}'
```
**Penjelasan Kode :**
- ``cat`` merupakan command-line utility pada Linux yang digunakan untuk menampilkan isi pada suatu file. ``cat`` disini digunakan untuk mengakses data pada ``2023 QS World University Rankings.csv``
- ``grep`` merupakan command-line utility di Linux yang digunakan untuk mencari pola dari sebuah text file. Disini kita mencari string ``"Japan"`` dari file .csv yang sebelumnya kita akses menggunakan ``cat``.
- ``sort`` merupakan command pada Linux yang digunakan untuk mengurutkan barisan pada text file pada urutan tertentu. ``-t ,`` merupakan command yang terdapat pada ``sort`` yang menandakan bahwa data dipisah oleh separator comma. ``-k20`` berarti melakukan sorting dari seluruh kolom 20 pada .csv filenya. ``-n`` artinya melakukan sorting secara numerikal, dan secara default akan melakukannya secara ascending.
- ``head`` merupakan command pada Linux untuk menampilkan beberapa line pertama dari sebuah file atau command. ``-n 10`` disini berarti mengambil 10 baris pertama dari input.
- ``awk`` merupakan command pada Linux untuk memproses dan manipulasi text file. ``-F ,`` merupakan command pada ``awk`` untuk mendetermine separator dari data yang terdapat pada file dan separator disini adalah comma. Kemudian ``print`` digunakan untuk menampilakan data sesuai dengan kondisi-kondisi yang ada, ``$1, $2, $4, $20`` berarti mengambil data dari kolom ke-1, 2, 4 dan 20.

### Soal D
Bocchi ngefans berat dengan universitas **paling keren** di dunia. **_Bantu bocchi mencari universitas tersebut dengan kata kunci keren._**

**Solusi :**
```sh
cat '2023 QS World University Rankings.csv' | awk -F , '/Keren/ {print $1, $2}'
```
**Penjelasan Kode :**
- ``cat`` merupakan command-line utility pada Linux yang digunakan untuk menampilkan isi pada suatu file. ``cat`` disini digunakan untuk mengakses data pada ``2023 QS World University Rankings.csv``
- ``awk`` merupakan command pada Linux untuk memproses dan manipulasi text file. ``-F ,`` merupakan command pada ``awk`` untuk mendetermine separator dari data yang terdapat pada file dan separator disini adalah comma. ``/Keren/`` digunakan untuk mencari baris yang mengandung string "Keren", kemudian di print data-data yang ada pada kolom ke-1 dan 2 menggunakan``{print $1, $2}``.

**Jawaban:**
```sh
#!/bin/bash
#1a
echo ==========1a==========
grep "Japan" '2023 QS World University Rankings.csv'| sort -t , -k1 -n| head -n 5| awk -F , '{print $1, $2, $4}'
echo 

#1b
echo ==========1b==========
cat '2023 QS World University Rankings.csv' | grep "Japan" | sort -t , -k9 -n| head -n 5| awk -F , '{print $1, $2, $4, $9}'
echo 

#1c
echo ==========1c==========
cat '2023 QS World University Rangkings.csv' | grep "Japan" | sort -t , -k20 -n| head -n 10| awk -F , '{print $1, $2, $4, $20}'
echo 

#1d
echo ==========1d==========
cat '2023 QS World University Rankings.csv' | awk -F , '/Keren/ {print $1, $2}'
echo
```

**Screenshot Output Kode-Kode Nomor 1 :**
| <p align="center"> university_survey.sh </p> |
| -------------------------------------------- |
| <img src="/uploads/abc7cf20a50e302e588a5e532f9e21f1/no1.png" width = "350"/> |

## Nomor 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

### Soal A
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 

**Solusi :**
- Menyimpan jumlah folder yang bernama kumpulan_(angka) ke dalam sebuah variable
```sh
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')
```
- ``ls`` untuk melihat daftar folder dan file yang ada pada directory tersebut. Selanjutnya, dengan command ``grep -c`` kita akan mencari berapa file atau folder yang mengandung string ``kumpulan_[0-9]``. ``[0-9]`` berarti suatu angka, simplenya, pada grep mencari jumlah file atau folder yang mengandung kumpulan_(angka). Setelah kita mengetahui jumlah folder yang mengandung string kumpulan_(angka) pada directory, kita akan mengembalikan jumlah tersebut + 1 dengan command ``awk '{print $1+1}'`` ke variable ``folder_count``.
- Simpan nama folder ke suatu variable, kemudian buat direktori baru untuk folder tersebut.
```sh
folder_name="/home/arvianta/sisop/P1/NO2/kumpulan_${folder_count}"
mkdir $folder_name
```
- Mendapatkan jam sekarang dan dimasukkan ke dalam suatu variable untuk menentukan berapa kali kita akan mendownload gambar.
```sh
current_hour=$(date +"%H")

if [ $current_hour -eq 0 ]; then
	num_images=1
else
	num_images=$current_hour
fi
```
- Pengambilan jam saat ini dengan menggunakan fungsi ``(date + "%H")``. Kemudian, sesuai dengan ketentuan soal, apabila jam sekarang adalah 00:00, cukup download 1 file saja. Maka dari itu dibuatlah if-else seperti kode di atas.
- Setelah mendapatkan jam saat ini, dilakukan perulangan untuk mendownload file gambar dari internet serta memberi nama ``perjalanan_`` kepada file tersebut.
```sh
file_num=1

for ((file_num; file_num <= $num_images; file_num++))
do 
	filename="perjalanan_$file_num.file"
	wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
```
- Pada potongan program tersebut, kita deklarasikan variable ``file_num=1`` untuk melakukan perulangan. Pada for loop, dideklarasikan variable ``filename="perjalanan_$file_num.file"`` untuk memberikan nama pada file yang akan didownload nanti. Nama akan mengikuti perulangan, sebagai contoh saat ini ``file_num=1``, maka file akan bernama ``perjalanan_1.file``. Kemudian, kita mendownload file dari internet dengan menggunakan command ``wget -O "$folder_name/$filename" "link"``. ``-O`` artinya output, maka file yang telah terdownload akan di output ke ``"$folder_name/$filename"``. Sebagai contoh, ``/home/arvianta/sisop/P1/NO2/kumpulan_1/perjalanan_1.file``. Perulangan akan dilakukan sebanyak ``num_images`` kali, yang berisi nominal jam sekarang.
- Semua potongan kode di atas dikumpulkan ke sebuah fungsi
```sh
kumpuler(){
# Create folder name kumpulan
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')
folder_name="/home/arvianta/sisop/P1/NO2/kumpulan_${folder_count}"
mkdir $folder_name

# Store current hour
current_hour=$(date +"%H")

if [ $current_hour -eq 0 ]; then
	num_images=1
else
	num_images=$current_hour
fi

# Download and create file perjalanan_
file_num=1

for ((file_num; file_num <= $num_images; file_num++))
do 
	filename="perjalanan_$file_num.file"
	wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
}
```
Pendownloadan gambar hanya dilakukan setiap 10 jam sekali, hal ini dilakukan menggunakan cronjob dengan syntax berikut.
```sh
crontab -e
```
Kemudian dengan script cronjob berikut
```sh
0 */10 * * *  /bin/bash /home/arvianta/P1/NO2/kobeni_liburan.sh kumpuler
```

### Soal B
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

**Solusi :**
- Menyimpan jumlah folder atau file yang bernama devil_(angka) ke dalam sebuah variable
```sh
zip_count=$(ls | grep -c '^devil_[0-9]*$' | awk '{print $1+1}')
```
- ``ls`` untuk melihat daftar folder dan file yang ada pada directory tersebut. Selanjutnya, dengan command ``grep -c`` kita akan mencari berapa file atau folder yang mengandung string ``devil_[0-9]``. ``[0-9]`` berarti suatu angka, simplenya, pada grep mencari jumlah file atau folder yang mengandung devil_(angka). Setelah kita mengetahui jumlah file yang mengandung string devil_(angka) pada directory, kita akan mengembalikan jumlah tersebut + 1 dengan command ``awk '{print $1+1}'`` ke variable ``zip_count``.
- enyimpan jumlah folder atau file yang bernama kumpulan_(angka) ke dalam sebuah variable
```sh
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1}')
```
- Teknis pencarian jumlah folder kumpulan sama saja dengan pencarian jumlah file devil, namun disini tidak dijumlah dengan 1 karena hanya dibutuhkan pada perulangan zipping nanti, bukan untuk membuat folder kumpulan baru seperti pada zip.
- Simpan nama zip ke suatu variable
```sh
zip_name="/home/arvianta/sisop/P1/NO2/devil_${zip_count}"
```
- Lakukan perulangan untuk melakukan zipping folder kumpulan
```sh
for ((i=1; i <= folder_count; i++)) do
	zip -r $zip_name.zip kumpulan_$i
	rm -r /home/arvianta/sisop/P1/NO2/kumpulan_$i
done
```
- Perulangan dilakukan sebanyak folder_count kali, kemudian zipping dilakukan dengan command ``zip -r $zip_name.zip kumpulan_$i`` dimana folder kumpulan ke-i akan di zip ke dalam ``$zip_name.zip`` (contoh: devil_1.zip). Setelah melakukan zipping, folder akan dihapus untuk menghemat tempat penyimpanan pada komputer (sesuai soal). Penghapusan folder menggunakan command ``rm -r``.
- Semua potongan kode di atas dikumpulkan ke sebuah fungsi
```sh
zipper(){
zip_count=$(ls | grep -c '^devil_[0-9]*$' | awk '{print $1+1}')
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1}')
zip_name="/home/arvianta/sisop/P1/NO2/devil_${zip_count}"
	
for ((i=1; i <= folder_count; i++)) do
	zip -r $zip_name.zip kumpulan_$i
	rm -r /home/arvianta/sisop/P1/NO2/kumpulan_$i
done
}
```
Zipping dilakukan setiap jam 12 malam menggunakan cronjob dengan syntax berikut.
```sh
crontab -e
```
Kemudian dengan script cronjob berikut
```sh
0 0 * * *  /bin/bash /home/arvianta/P1/NO2/kobeni_liburan.sh zipper
```
**Screenshot Output Kode-Kode Nomor 2 :**
| <p align="center"> kobeni_liburan.sh kumpuler </p> | <p align="center"> kobeni_liburan.sh zipper </p> |
| -------------------------------------- | -------------------------------------- |
| <img src="/uploads/ea5d4cf6440d0df745cc61b731baee95/no2.png" width = "500"/> | <img src="/uploads/f9eb5c9cf34a68c7b145bc7052e6448a/no2b.png" width = "500"/> |

## Nomor 3
Peter Griffin hendak membuat suatu sistem register pada script **louis.sh** dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script **retep.sh**
### Soal A
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
-	Minimal 8 karakter
-	Memiliki minimal 1 huruf kapital dan 1 huruf kecil
-	Alphanumeric
-	Tidak boleh sama dengan username 
-	Tidak boleh menggunakan kata chicken atau ernie

**Solusi:**
```sh
mkdir -p "users"
list_user="users/users.txt"
function log {
 	echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

echo "=====Registration System====="
echo "Input new username and password"

read -p "Username: " username
read -p "Password: " password
```
-	**Pertama**, membuat sistem pendaftaran yang meminta user untuk memasukkan nama user dan password baru. Kemudian, membuat direktori ``users`` menggunakan ``mkdir -p``. Kemudian mengatur variabel ``list_user`` dengan ``users.txt``.

	``log.txt`` mengambil parameter nama user dan password dan menambahkan baris ke file ``log.txt`` yang mencakup tanggal dan waktu, kata ``REGISTER: ``, nama user, serta password. Terakhir, program meminta user untuk memasukkan nama user dan password menggunakan perintah ``read``, lalu memanggil fungsi ``log`` untuk mencatat.

	**Untuk input user supaya pada saat memasukkan password tidak terlihat, dapat menggunakan command ``-sp`` 

```sh
if [[ "$(grep "$username" "$list_user")" ]]; then
	echo -e "\nUsername already registered"
	log "ERROR" "User already exists"
exit 1
```
-	**Kedua**, program akan memeriksa apakah nama user yang dimasukkan oleh user sudah terdaftar atau belum. ``grep`` digunakan untuk mencari string yang cocok dengan nama user di dalam file ``users.txt`` yang ditunjukkan oleh ``$list_user``.

	Jika ``grep`` menemukan nama user yang cocok, maka ``if`` akan mencetak "_Username already registered_", serta memanggil ``log`` dengan parameter "_ERROR_" dan "_User already exists_".

```sh
elif [[ "${#password}" -lt 8 ]]; then
	echo -e "\nPassword must contain at least 8 characters"
exit 1
```

-	**Ketiga**, program akan memeriksa apakah password memenuhi syarat yaitu terdiri dari minimal 8 karakter.

	Pengecekan ini dilakukan dengan menggunakan operator ``-lt 8`` yang memeriksa apakah panjang string password kurang dari 8. Jika iya, maka program akan mencetak "_Password must contain at least 8 characters_" dan kemudian keluar menggunakan ``exit 1``.

```sh
elif [[ ! $(echo "$password" | grep '[A-Z]' | grep '[a-z]') ]]; then
	echo -e "\nPassword must contain at least 1 uppercase and 1 lowercase alphabet"
exit 1
```
-	**Keempat**, program akan memeriksa apakah password mengandung minimal satu huruf besar dan satu huruf kecil. Pengecekan ini dilakukan menggunakan _echo_ untuk input password dan menjalankan perintah ``grep ’[A-Z]’`` serta ``grep '[a-z]'`` untuk mencari huruf besar dan huruf kecil.

	Jika tidak ditemukan huruf besar dan huruf kecil, maka program akan mencetak "_Password must contain at least 1 uppercase and 1 lowercase alphabet_" dan kemudian keluar dari program menggunakan perintah ``exit 1``.

```sh
elif [[ "$password" =~ [^a-zA-Z0-9] ]]; then
	echo -e "\nPassword must only contain alphanumeric characters"
exit 1
```
-	**Kelima**, program akan memeriksa apakah password mengandung _alphanumeric_ dengan menggunakan perintah ``=~`` dan memeriksa apakah password mengandung karakter selain huruf besar, huruf kecil, dan angka.

	Jika iya, maka program akan mencetak "_Password must only contain alphanumeric characters_" dan kemudian keluar dari program menggunakan perintah ``exit 1``.

```sh
elif [[ "$password" == "$username" ]]; then
	echo -e "\nPassword and username cannot be the same"
exit 1
```
-	**Keenam**, program akan memeriksa apakah password sama dengan nama user menggunakan ``"$password"=="$username"``.

	Jika password sama dengan nama user, maka program akan mencetak "_Password and username cannot be the same_" dan kemudian keluar dari program menggunakan perintah ``exit 1``.

```sh
elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $``(echo "$password" | grep -i 'ernie') ]]; then
	echo -e "\nPassword cannot contain the word 'chicken' or 'ernie'"
exit 1
```
-	**Ketujuh**, program akan memeriksa apakah password mengandung kata “_chicken_” atau “_ernie_” menggunakan _grep_.

	Jika iya, maka program akan mencetak "_Password cannot contain the word 'chicken' or 'ernie'_ " dan kemudian keluar dari program menggunakan perintah ``exit 1``.

```sh
else
	echo "$username:$password" >> "$list_user"
	echo -e "\nAccount has been created"
	log "INFO" "User $username registered succesfully"
fi
```
-	**Terakhir**, program akan menambahkan nama user dan password ke dalam _users.txt_ dengan format ``$username:_ _$password``. Lalu, program akan mencetak "_Account has been created_".

	Selanjutnya, program akan memanggil fungsi ``log "INFO" "User $username registered succesfully"`` dan tersimpan ke dalam ``log.txt``.

	Program akan keluar menggunakan perintah ``fi``. 

**Jawaban:**
```sh
mkdir -p "users"

list_user="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

echo "=====Registration System====="
echo "Input new username and password"

read -p "Username: " username
read -p "Password: " password

if [[ "$(grep "$username" "$list_user")" ]]; then
	echo -e "\nUsername already registered"
	log "ERROR" "User already exists"
exit 1

elif [[ "${#password}" -lt 8 ]]; then
	echo -e "\nPassword must contain at least 8 characters"
exit 1

elif [[ ! $(echo "$password" | grep '[A-Z]' | grep '[a-z]') ]]; then
	echo -e "\nPassword must contain at least 1 uppercase and 1 lowercase alphabet"
exit 1

elif [[ "$password" =~ [^a-zA-Z0-9] ]]; then
	echo -e "\nPassword must only contain alphanumeric characters"
exit 1

elif [[ "$password" == "$username" ]]; then
	echo -e "\nPassword and username cannot be the same"
exit 1

elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
	echo -e "\nPassword cannot contain the word 'chicken' or 'ernie'"
exit 1

else
	echo "$username:$password" >> "$list_user"
	echo -e "\nAccount has been created"
	log "INFO" "User $username registered succesfully"
fi
```

### Soal B
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
-	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
-	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
-	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
-	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

**Solusi:**
```sh
list_user="users/users.txt"

function log {
echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

echo "=====Login System====="
echo "Input Username and Password"

read -p "Username: " username
read -p "Password: " password
```
-	**Pertama**, buat sistem login yang membaca daftar user dari ``users/users.txt`` dan menuliskan aktivitas login ke "log.txt".

	Fungsi ``log`` digunakan untuk menulis aktivitas login ke ``log.txt``. Fungsi ini menerima username dan password, dan menuliskannya ke dalam ``log`` dengan format "_tanggal dan waktu aktivitas LOGIN username password_ ".

	Program kemudian meminta user untuk memasukkan ``username`` dan ``password``.

	Setelah itu, program akan memeriksa apakah ``username`` dan ``password`` tersebut terdapat di dalam ``users/users.txt``. Jika cocok, program akan menuliskan aktivitas login menggunakan fungsi ``log``.


```sh
if [[ ! "$(grep "$username" "$list_user")" ]] ; then
	echo -e "\nIncorrect Username or password"
exit 1
fi
```

-	**Kedua**, mengecek apakah username ada dalam file ``users/users.txt``. Jika tidak ada, maka program akan menampilkan pesan "_Incorrect Username or Password_" dan keluar dari program dengan ``exit 1``.

	Program akan mencari kecocokan antara variabel ``username`` yang diinput oleh user dengan yang ada di ``users/users.txt`` menggunakan perintah ``grep “$username” “$list_user``.

	Jika tidak ditemukan, maka program akan menampilkan pesan "_Incorrect Username or Password_" dan keluar dari program dengan perintah ``exit 1``.


```sh
if [[ "$(grep "$username" "$list_user" | cut -d ':' -f 2)" == "$password" ]] ; then
	echo -e "\nLogin Successful"
log "INFO" "User $username logged in"
```


-	**Ketiga**, bagian ini akan mengecek apakah username dan password yang dimasukkan cocok dengan yang ada di ``users/users.txt`` dengan menggunakan ``grep``. Jika cocok, maka program akan menampilkan pesan "_Login Successful_" dan memanggil fungsi ``log "INFO" "User $username logged in"`` untuk mencatat aktivitas login ke ``log.txt``.

	Setelah cocok, program menggunakan perintah ``cut`` untuk memotong bagian kedua dari hasil pencarian ``password`` menggunakan ``:``.

	Lalu program akan menampilkan pesan “_Login Successful_" dan mencatat aktivitas login ke "log.txt" menggunakan perintah ``log "INFO" "User $username logged in"``.
 

```sh
else
	echo -e "\nLogin Failed"
	log "ERROR" "Failed attempt on user $username"
exit 1
fi
```

-	**Terakhir**,  jika proses verifikasi ``username`` dan ``password`` tidak berhasil, program akan menampilkan pesan "_Login Failed_", mencatat aktivitas gagal login, dan keluar dari program dengan perintah ``exit 1``.

	Program akan menampilkan pesan "_Login Failed_" serta memanggil fungsi ``log "ERROR" "Failed attempt on user $username"`` untuk mencatat aktivitas gagal login.

**Jawaban:**
```sh
list_user="users/users.txt"

function log {
 	echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

echo "=====Login System====="
echo "Input Username and Password"

read -p "Username: " username
read -p "Password: " password

if [[ ! "$(grep "$username" "$list_user")" ]] ; then
	echo -e "\nIncorrect Username or password"
	exit 1
fi

if [[ "$(grep "$username" "$list_user" | cut -d ':' -f 2)" == "$password" ]] ; then
	echo -e "\nLogin Successful"
	log "INFO" "User $username logged in"
else
	echo -e "\nLogin Failed"

log "ERROR" "Failed attempt on user $username"
exit 1
fi
```

**Screenshot Output Kode-Kode Nomor 3 :**
| <p align="center"> Screenshot </p> | <p align="center"> file source </p> |
| ---------------------------------- | ----------------------------------- |
| <img src="/uploads/6eee2ec11df3f5874d8c83b126958545/no3a.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/b3f88fa34b2e34881d1ae74d44eb98f4/no3b.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/4695a7a5ae29225ef5f2fb9feaf4725e/no3c.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/fb401007d19480138997dc9964ca03bb/no3d.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/7ae22f3b7a5cf74c2a4845e8c899af2a/no3e.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/4358d3cca4a491c91f9c2a9cdf0172e5/no3f.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/58e650ad47fd78bd57dd7df039dc0474/no3g.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/3deda8f31c7c1e44bbc19e4d6c6e5193/no3h.png" width = "350"/> | <p align="center"> louis.sh </p> |
| <img src="/uploads/2e515b094544490422832ddf3dcb4755/no3i.png" width = "350"/> | <p align="center"> retep.sh </p> |
| <img src="/uploads/dcb8219371ac9a9ddd6b3f641d5ddb79/no3j.png" width = "350"/> | <p align="center"> retep.sh </p> |



## Nomor 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
	- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
	- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
	- Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

**Solusi :**
### Encrypting
- Pertama, buat directory baru berupa folder dimana semua .txt file enkripsi dari syslog akan disimpan.
```sh
mkdir -p "encrypted_backup"
```
- Kemudian, deklarasi variable untuk menyimpan alfabet a-z dan A-Z sebanyak 2 kali. Hal ini dilakukan supaya pada saat melakukan enkripsi, setelah z bisa kembali lagi ke a.
```sh
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
```
- Simpan nilai dari jam saat ini untuk melakukan enkripsi, serta jam dan tanggal saat ini untuk menamakan file .txt yang berisi hasil dari enkripsi syslog, masing-masing ke dalam sebuah variable.
```sh
curr_hour=$(date "+%H")
curr_date=$(date "+%H:%M %d:%m:%Y")
```
- Pindah ke direktori ``encrypted_backup`` 
```sh
cd encrypted_backup
```
- Deklarasi variable untuk menyimpan nama dari file .txt yang terdapat hasil enkripsi dari syslog.
```sh
backup_filename="${curr_date}.txt"
```
- Akses syslog dan lakukan enkripsi
```sh
cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${curr_hour}:26}${uppercase:${curr_hour}:26}" > "${backup_filename}"
```
- Pada baris kode tersebut, kita mengakses syslog dengan menggunakan command ``cat``. Kemudian, kita lakukan enkripsi dengan command ``tr``, dimana kita mentranslate text pada syslog yang mengandung ``${lowercase:0:26}${uppercase:0:26}`` (string lowercase dan uppercase, dari index ke-0, diambil 26 elemen, yang artinya a-z, dan A-Z), kita translate menjadi ``${lowercase:${curr_hour}:26}${uppercase:${curr_hour}:26}``(string lowercase dan uppercase, dari index ke-curr_hour, diambil 26 elemen). Kemudian hasilnya kita output ke file.txt yang namanya tersimpan di variable ``backup_filename``.

Encrypting dilakukan setiap 2 jam sekali menggunakan cronjob dengan syntax berikut.
```sh
crontab -e
```
Kemudian dengan script cronjob berikut
```sh
0 */2 * * * /bin/bash /home/arvianta/sisop/P1/NO4/log_encrypt.sh
```

### Decrypting
- Pertama, buat directory baru berupa folder dimana semua .txt file dekripsi dari hasil enkripsi syslog akan disimpan.
```sh
mkdir -p "decrypted_backup"
```
- Kemudian, deklarasi variable untuk menyimpan alfabet a-z dan A-Z sebanyak 2 kali. Hal ini dilakukan supaya pada saat melakukan dekripsi, setelah z bisa kembali lagi ke a.
```sh
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
```
- Prompt user untuk menginput nama file enkripsi syslog yang akan di dekripsi.
```sh
read -p "Insert file name : " filename
``` 
- Membuat variable untuk menyimpan value jam pada file yang terenkripsi
```sh
filehour=$(echo "$filename" | cut -d ':' -f1) 
``` 
- Pada kode tersebut, kita memasukkan filename ke filehour dengan menggunakan command ``echo``, kemudian ambil jamnya saja dengan command ``cut``. ``-d ':'`` pada ``cut`` menandakan pemisah dari input yaitu tanda titik dua, kemudian diambil field pertama sebelum pemisah titik dua menggunakan ``-f1``.
- Setelah mendapatkan jam pada file terenkripsi tersebut, pindah direktori ke encrypted_backup
```sh
cd encrypted_backup
```
- Simpan path file dari file encrypted dengan menggunakan command ``pwd``
```sh
encrypted_path="$(pwd)"/"${filename}"
```
- Pindah ke direktori decrypted_backup, dimana file.txtnya akan disimpan
```sh
cd ..

cd decrypted_backup
```
- Lakukan dekripsi pada file yang terenkripsi
```sh
cat "${encrypted_path}" | tr "${lowercase:${filehour}:26}${uppercase:${filehour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${filename}"
```
- Pada baris kode tersebut, kita mengakses file yang terenkripsi dengan menggunakan command ``cat``. Kemudian, kita lakukan dekripsi dengan command ``tr``, dimana kita akan mentranslate text pada encrypted file yang mengandung ``${lowercase:${filehour}:26}${uppercase:${filehour}:26}``(string lowercase dan uppercase, dari index ke-filehour, diambil 26 elemen), kita translate menjadi ``${lowercase:0:26}${uppercase:0:26}``(string lowercase dan uppercase, dari index ke-0, diambil 26 elemen). Kemudian hasilnya kita output ke file.txt yang namanya tersimpan di variable ``filename``.

**Jawaban:**
- log_encrypt.sh
```sh
#!/bin/bash

mkdir -p "encrypted_backup"

uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

curr_hour=$(date "+%H")
curr_date=$(date "+%H:%M %d:%m:%Y")

cd encrypted_backup

backup_filename="${curr_date}.txt"

cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${curr_hour}:26}${uppercase:${curr_hour}:26}" > "${backup_filename}"
```
- log_decrypt.sh
```sh
#!/bin/bash

mkdir -p "decrypted_backup"

uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

read -p "Insert file name : " filename

filehour=$(echo "$filename" | cut -d ':' -f1) 

cd encrypted_backup

encrypted_path="$(pwd)"/"${filename}"

cd ..

cd decrypted_backup

cat "${encrypted_path}" | tr "${lowercase:${filehour}:26}${uppercase:${filehour}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${filename}"
```
**Screenshot Output Kode-Kode Nomor 4 :**
| <p align="center"> log_encrypt.sh </p> | <p align="center"> log_decrypt.sh </p> |
| -------------------------------------- | -------------------------------------- |
| <img src="/uploads/36a05e603a810684291b573e7819e4b3/no4_encrypt.png" width = "500"/> | <img src="/uploads/3ca16c2ebaad05fcfd7bc0373ae41db9/no4_decrypt.png" width = "500"/> |
