#!/bin/bash

zipper(){
zip_count=$(ls | grep -c '^devil_[0-9]*$' | awk '{print $1+1}')
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1}')
zip_name="/home/arvianta/sisop/P1/NO2/devil_${zip_count}"
	
for ((i=1; i <= folder_count; i++)) do
	zip -r $zip_name.zip kumpulan_$i
	rm -r /home/arvianta/sisop/P1/NO2/kumpulan_$i
done
}

kumpuler(){
# Create folder name kumpulan
folder_count=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')
folder_name="/home/arvianta/sisop/P1/NO2/kumpulan_${folder_count}"
mkdir $folder_name

# Store current hour
current_hour=$(date +"%H")

if [ $current_hour -eq 0 ]; then
	num_images=1
else
	num_images=$current_hour
fi

# Download and create file perjalanan_
file_num=1

for ((file_num; file_num <= $num_images; file_num++))
do 
	filename="perjalanan_$file_num.file"
	wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
}

if [ "$1" = "kumpuler" ]; then
  kumpuler
elif [ "$1" = "zipper" ]; then
  zipper
fi

#Crontab
# 0 */10 * * *  /bin/bash /home/arvianta/P1/NO2/kobeni_liburan.sh kumpuler
# 0 0 * * * /bin/bash /home/arvianta/sisop/P1/NO2/kobeni_liburan.sh zipper
